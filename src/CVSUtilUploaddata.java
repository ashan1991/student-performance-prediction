/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ashan
 */
import views.CSVUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.Arrays;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.NonSparseToSparse;
import java.util.Scanner;

public class CVSUtilUploaddata {
    public static void main(String[] args) throws Exception {
        Scanner user_input = new Scanner(System.in);
        
        String Dalc,Walc,avg_wal,G1,G2,G3,average, r_o_a, p_a, rpa, status;
        System.out.println("Enter Dalc");
        Dalc = user_input.next();
        
        System.out.println("Enter Walc");
        Walc = user_input.next();
        
        System.out.println("Enter avg_wal");
        avg_wal = user_input.next();
        
        System.out.println("Enter G1");
        G1 = user_input.next();
        
        System.out.println("Enter G2");
        G2 = user_input.next();
        
        System.out.println("Enter G3");
        G3 = user_input.next();
        
        System.out.println("Enter average");
        average = user_input.next();
        
        System.out.println("Enter r_o_a");
        r_o_a = user_input.next();
        
        System.out.println("Enter p_a");
        p_a = user_input.next();
        
        System.out.println("Enter rpa");
        rpa = user_input.next();
        
        System.out.println("Enter status");
        status = user_input.next();
        
        
        
        
        
        
        
        String csvFile = "src\\data\\predictdata.csv";
        FileWriter writer = new FileWriter(csvFile);
        
        CSVUtils.writeLine(writer, Arrays.asList("Dalc numeric", "Walc numeric", "avg. wal", "G1", "G2", "G3", "Average", "Round of Average", "Persantage Average", "Round Persantage Average", "Status"));

        //double-quotes
        CSVUtils.writeLine(writer, Arrays.asList(Dalc, Walc, avg_wal, G1, G2, G3, average, r_o_a, p_a, rpa, status));

        CSVLoader loader = new CSVLoader();
        loader.setSource(new FileInputStream(new File("src\\data\\predictdata.csv")));
        Instances data = loader.getDataSet();//get instances object

        NonSparseToSparse sp = new NonSparseToSparse();

       //specify the dataset
        sp.setInputFormat(data);
        //apply
        Instances newData = Filter.useFilter(data, sp);

        
        
        
        
        // save ARFF
        ArffSaver saver = new ArffSaver();
        saver.setInstances(newData);//set the dataset we want to convert
        //and save as ARFF
        saver.setFile(new File("src\\data\\student-por-pred.arff"));
        saver.writeBatch();
        
        
        writer.flush();
        writer.close();
        
        

    }
}
