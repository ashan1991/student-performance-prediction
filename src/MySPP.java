import views.CSVUtils;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Discretize;
import weka.filters.unsupervised.instance.NonSparseToSparse;
import java.io.File;
import java.io.FileInputStream;
import java.util.Scanner;
import weka.core.FastVector;
import weka.classifiers.functions.LinearRegression;
import weka.classifiers.meta.MultiClassClassifier;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import views.MainFrame;
//import static views.MainFrame.sup;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.classifiers.trees.RandomTree;
import weka.classifiers.rules.PART;
import weka.core.FastVector;
import java.io.FileWriter;
import java.util.Arrays;

/**
 * Created by ashan on 2/07/2017.
 */
public class MySPP {

    public static void main(String[] args) throws Exception {
        

        createArffFromCsvToSparse();

     //   discretizeAttributes();

//discretizeAttributes();
      //printDefaultAccuracy();
      
      
 //     pred();
      
      //  printj48class();
      //  printMulticlass();
        
    //    createcsv();
        
       // getoutput();
        
        //printmultivalueaccurace();
        //discretizeAttributes();

    }

    private static void discretizeAttributes() throws Exception {
        ConverterUtils.DataSource source = new ConverterUtils.DataSource("src\\new_set\\student-por-train.arff");
        Instances dataset = source.getDataSet();

        String[] options = new String[2];
        //choose the number of intervals, e.g. 2 :
        options[0] = "-B";
        options[1] = "5";
        Discretize discretize = new Discretize();
        discretize.setOptions(options);
        discretize.setInputFormat(dataset);
        Instances newData = Filter.useFilter(dataset, discretize);
        ArffSaver saver = new ArffSaver();
        saver.setInstances(newData);
        saver.setFile(new File("src\\new_data\\student-por-train-discretize.arff"));
        saver.writeBatch();
    }
    
    private static void pred() throws Exception {
        ConverterUtils.DataSource source = new ConverterUtils.DataSource("src\\new_set\\student-por-train.arff");
        Instances trainDataset = source.getDataSet();

        trainDataset.setClassIndex(trainDataset.numAttributes()-1);

        RandomTree smo01 = new RandomTree();
        smo01.buildClassifier(trainDataset);
        
        PART smo02 = new PART();
        smo02.buildClassifier(trainDataset);   
        
        
        ConverterUtils.DataSource source1 = new ConverterUtils.DataSource("src\\new_set\\student-por-test.arff");
        Instances testDataset = source1.getDataSet();

        testDataset.setClassIndex(testDataset.numAttributes()-1);

        int total = 0;

        System.out.println("Predicted class");
        
 
     
        for (int i = 0; i < testDataset.numInstances(); i++) {

            double   actualValue = testDataset.instance(i).classValue();


            Instance newInst = testDataset.instance(i);

           // double[] c = lr.getMembershipValues(newInst);
            double predSMO = smo01.classifyInstance(newInst);

            System.out.println(actualValue+", "+predSMO);
            
              if(actualValue == predSMO){
                total++;
            }
            
            
        }
        
        System.out.printf("Accuracy: %2.5f", (total * 1.0 / testDataset.numInstances())*100 );
        System.out.println('\n');
        

        System.out.println('\n');

    }

    
    private static void createArffFromCsvToSparse() throws Exception {

        CSVLoader loader = new CSVLoader();
        loader.setSource(new FileInputStream(new File("src\\Newfolder\\student-por-train.csv")));
        Instances data = loader.getDataSet();//get instances object

        NonSparseToSparse sp = new NonSparseToSparse();

        //specify the dataset
        sp.setInputFormat(data);
        //apply
        Instances newData = Filter.useFilter(data, sp);

        // save ARFF
        ArffSaver saver = new ArffSaver();
        saver.setInstances(newData);//set the dataset we want to convert
        //and save as ARFF
        saver.setFile(new File("src\\Newfolder\\student-por-train.arff"));
        saver.writeBatch();
    }
    
    private static void createcsv() throws Exception {
   // Scanner user_input = new Scanner(System.in);
        
        String studytime, failures, schoolsup, higher, goout, Dalc, Walc, absences, status;
        System.out.println("studytime");
        studytime = "1";
        
        System.out.println("failure");
        failures = "1";
        
        System.out.println("schoolsup");
        schoolsup = "1";
        
        System.out.println("higher");
        higher = "1";
        
        System.out.println("goout");
        goout = "1";
        
        System.out.println("dalc");
        Dalc = "1";
        
        System.out.println("walc");
        Walc = "1";
        
        System.out.println("absences");
        absences = "1";
        
        status = "?";

        String csvFile = "src\\new_set\\predictdata.csv";
        FileWriter writer = new FileWriter(csvFile);
        
        CSVUtils.writeLine(writer, Arrays.asList("studytime", "failures", "schoolsup", "higher", "goout", "Dalc", "Walc", "absences", "Status"));

        //double-quotes
        CSVUtils.writeLine(writer, Arrays.asList(studytime, failures, schoolsup, higher, goout, Dalc, Walc, absences, status));
        
        
        writer.flush();
        writer.close();

       CSVLoader loader = new CSVLoader();
        loader.setSource(new FileInputStream(new File("src\\new_set\\predictdata.csv")));
        Instances data = loader.getDataSet();//get instances object

        NonSparseToSparse sp = new NonSparseToSparse();

       //specify the dataset
        sp.setInputFormat(data);
        //apply
        Instances newData = Filter.useFilter(data, sp);

        
    
        ArffSaver saver = new ArffSaver();
        saver.setInstances(newData);//set the dataset we want to convert

        saver.setFile(new File("src\\new_set\\predictdata.arff"));
        saver.writeBatch();
        
   
    
    }
    
    
    
    
    

    }
    


